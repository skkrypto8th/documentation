# Elliptic Curve Cryptography: ECDH and ECDSA #

---
 
## Domain Parameters ##

여기서 다루는 타원곡선 알고리즘들은 타원곡선 위 유한체(finite field)의 순환군(cyclic subgroup) 내에서 이뤄진다.
때문에 다음과 같은 매개 변수들을 정의한다.

- 소수 p는 유한체의 크기를 정의한다. (The prime p that specifies the size of the finite field.)
- 계수 a와 b는 타원곡선을 정의한다. (The coefficients a and b of the elliptic curve equation.)
- 기준점(base point) G는 부분군을 정의한다. (The base point G that generates our subgroup.)
- n은 부분군의 위수(order)이다. cf, 위수는 유한군을 이루는 요소의 수 (The order n of the subgroup.)
- h는 부분군의 cofactor이다.

---

### cofactor ###
- 곡선 위의 점들은 h만큼의 순환군들로 나눠지고(partitions), 각각은 r을 위수로 갖는다.(각 부분군은 동일한 수의 점들을 갖는다.)
- 총 군(group)의 위수 n은 h*r로 정의한다. (부분군의 개수와 각 부분군의 점의 개수의 곱)
- 이때 부분군의 개수 h를 cofactor라고 정의한다.

![cofactior][logo]

[logo]: ../images/cofactor.png "cofactior"

ex) secp256k1: cofactor 1  
ex) ed255189: cofactor 8  
ex) curve448: cofactor 4  
---

## Random curves ##

이전 챕터에서 이산로그문제가 _어렵다_ 라는 말은 실은 틀렸다. 부분적으로 취약한 타원곡선을이 있으며, 이산로그문제를 효율적으로 해결할 수 있다고 알려진
알고리즘으로 문제를 풀 수 있다.
예를 들어, p=hn (유한체의 위수가 타원곡선의 위수와 같다)인 모든 곡선은 고전 컴퓨터에서 다항시간에 이산로그 문제를 해결할 수 있다고 알려진 [Smart's attack](https://wstein.org/edu/2010/414/projects/novotney.pdf) 에 취약하다.


### Smart's attach ###
- P = k*Q 인 k를 쉽게 구하는 공격
> 1. Fp 상의 점들을 p진수에 대한 집한 Qp로 옮길 수 있다.
> 2. Qp 위로 옮기면 E(Qp) 상의 점 X를 pZp에 매칭되는 어떤 값 x로 homomorphic하게 매핑할 수 있다.

우리가 곡선의 domain parameter를을 임의로 정의했을 때, 그 곡선은 '취약한' 곡선일 수 있고, 그 곡선 위의 이산로그문제를 쉽게 해결할 수 있는 알고리즘을 만들 수 있다.
그렇다면 이렇게 만든 곡선이 취약한지 어떻게 알 수 있을까  
**어떻게 해당 곡선이 안전함을 증명할 수 있을까.**

이런 문제를 풀기 위해, S(seed)라는 domain parameter를 추가로 사용한다.
S의 해시값으로 임의의 매개변수 a와 b 혹은 생섬점 G를 만든다.


![random-parameters-generation][random-parameters-generation]

[random-parameters-generation]: ../images/random-parameters-generation.png "random-parameters-generation"

![seed-inversion][seed-inversion]

[seed-inversion]: ../images/seed-inversion.png "seed-inversion"

seed로 만든 곡선은 verifiably random이라 한다. 해시를 통해 parameter를 만드는 원리는 [nothing up my sleeve](https://en.wikipedia.org/wiki/Nothing-up-my-sleeve_number) 로 알려져있다.

이 트릭은 *곡선을 만든 사람만 아는 취약성을 노출하면 안된다*는 약속을 보장해야한다. 만일 본인이 seed를 통해 곡선을 만들 경우, 본인은 곡선을 정의하는 a와 b를 고르는데 자유롭지 않으며, 타인들은 본인이 곡선을 특정 공격에 사용하지 않음을 어느 정도(relatively) 믿어야 한다.(you should be relatively sure that the curve cannot be used for special purpose attacks by me.) 왜 "어느 정도"라는 표현을 썼는지는 다음 장에 소개한다.


무작위 곡선을 만들고 검증할 때 사용하는 전형적인 알고리즘들은 ANSI X9.62에 정의되어 있고, SHA-1에 기초한다. 여기에 관심이 있다면, [SECG가 특정한](http://www.secg.org/sec1-v2.pdf) 검증가능한 무작위 곡선을 만드는 알고리즘을 읽으면 좋다.("Verifiably Random Curves and Base Point Generators" 부분을 찾아라)

---
## Elliptic Curve Cryptography ##

> 1. 개인키(piravet key)는 {1, ..., n-1}의 부분군에서 선택한 무작위 정수 d로 정의한다.(n은 부분군의 위수이다.)
> 2. 공개키(public key)는 타원곡선 위의 점 H = d*G로 정의한다.(G는 부분군의 생성점이다.)

YOU SEE? d와 G를 알고 있다면(다른 domain parameter들과 함께), 공개키 H를 계산하는 것은 '쉽다'.
하지만 H와 G를 알고 있을 때, 개인키 d를 계산하는 것은 이산로그문제를 해결해야하기 때문에 '어렵다'.

이것에 입각해, 두가키 공개키 알고리즘을 살펴볼 것이다:하나는 암호화에 사용하는 **ECDH(Elliptic curve Diffie-Hellman)** 이고, 하나는 디지털 서명에 사용하는 **ECDSA(Elliptic Curve Digital Signature Algorithm)** 이다.

---

## Encryption with ECHD ##

차후에 정리

---

## Signing with ECDSA ##

시나리오는 이렇다:Alice는 그녀의 개인키(dA)로 메세지에 서명하려 하고, Bob은 Alice의 공개키(HA)로 서명을 검증하려 한다. Alice만 개인키를 알고 있으며, 따라서 Alice만 유효한 서명 값을 만든다.
모든 사람은 서명값을 검증할 수 있다.

다시, Alice와 Bob은 동일한 domain parameter들을 사용한다. 우리가 볼 알고리즘은 ECDSA이며, 타원 곡선에 디지털 서명 알고리즘 적용한 사례 중 하나이다.

ECDSA는 메세지 자체가 아닌 메세지의 hash를 사용한다. 사용할 hash함수는 정하기 나름이며, 다만 암호학적으로 안전함이 보장된 해시함수([crpytographically-secure hash function](https://en.wikipedia.org/wiki/Cryptographic_hash_function)) 여야한다. 메세지의 해시값은 길이가 줄어들기 때문에(be truncated), 해시값의 길이는 부분군의 위수인 n과 같다.
truncated hash는 정수로 z라고 표현한다.

Alice가 서명하는 순서는 다음과 같다.
> 1. {1, ..., n-1}의 부분군에서 임의의 수 k를 선택한다.(k는 secret)
> 2. P = k*G인 점 P를 계산한다.
> 3. r = xp mod n인 xp를 계산한다.(xp는 점 P의 x좌표다.)
> 4. r이 0일 경우, k를 다시 정의하고 1-3을 반복한다.
> 5. s = k^-1(z+r*dA)mod n인 s를 계산한다.(dA는 Alice의 개인키이며, k^-1은 k의 곱셈 역원이다.)
> 6. s가 0일 경우, k다시 정의하고 1-5를 반복한다.

이 때 결과물인 r, s값이 서명값이다.

![ecdsa][ecdsa]

[ecdsa]: ../images/ecdsa.png "ecdsa"

쉽게 말로 풀어 설명하면,
먼저 비밀값인 k를 만든다. 이 비밀값에 점의 곱연산을 해 r값을 만든다. 이 r은 계산식 k^-1(z+r*dA)mod n 을 통해 메세지 해시값에 묶인다.

s을 계산하기 위해 우리는 k의 곱셈 역원을 계산한다. 지난 장에서 말했듯 n이 소수일 때만 가능하다. **따라서 부분군의 위수가 소수가 아니라면 ECDSA는 사용할 수 없다.**

---

## Verifying signatures ##
서명값을 증명하기 위해 우리는 Alice의 공개키 HA와 메세지 해시인 z, 서명값 (r,s)r값을 사용한다.

> 1. 정수 u1 = (s^-1)*z mod n 인 u1를 계산한다.
> 2. 정수 u2 = (s^-1)*4 mod n 인 u2를 계산한다.
> 3. 점 P = u1*G + u2*HA 인 P를 계산한다.
   서명값은 r = xp mod n 인 경우에만 유효하다.

---

## Correctnees of the Algorithm ##
P = u1*G + u2*HA 부터 시작하자. 공개키의 정의인 HA = dA*G로 다음과 같이 쓸 수 있다

    P = u1*G + u2*HA
      = u1*G + u2*(dA*G)
      = (u1 + u2*dA)G

u1과 u2의 정의로 다음과 같이 쓸 수 있다.

    P = (u1 + u2*dA)G
      = ((s^-1)*z + (s^-1)*r*dA)G
      = (s^-1)(z + r*dA)G


이전에 s = k^-1(z+r*dA)mod 라고 정의했다. 각각에 k를 곱하고 s를 나눠주면   
    
    k = s^-1(z+r*dA)mod

따라서  

    P = s^-1(z+r*dA)G
      = kG

이는 서명의 두번째 과정에서 구한 P와 동일하다. **서명값을 만들 때와 검증할 때 서로 다른 수식으로 동일한 점 P를 계산한다.**  
이 알고리즘이 작동하는 이유이다.

---

## The importance of k ##
ECDSA 서명 과정에서 k 값의 보안을 유지하는 것이 중요하다. 모든 서명에 동일한 k값을 사용하거나, 예측가능한, 취약한 무작위 숫자를 사용한다면 공격자는 개인키를 유추할 수 있다.  
[사례](https://www.bbc.com/news/technology-12116051)

이런 경우, 서명된 게임 2개를 산 후, 게임의 해시값과 각각의 서명값 그리고 domain parameter들로 쉽게 개인키 dS를 복구힐 수 있다.
> 1. 먼저 r = xp mod n 이고 P = k*G 이므로 r1 = r2 이다.
> 2. (s1 - s2) mod n = (k^-1)(z1 - z2) mod n 이다. (s의 정의에서 바로 유추할 수 있다.)
> 3. 양변에 k를 곱해 k(s1 - s2) mod n = (z1 - z2) mod n 를 얻는다.
> 4. 양변을 (s1 - s2)로 나눠 k = ((s1 - s2)^-1)(z1 - z2) mod n 를 얻는다. 
 
이렇게 각 해시값과 서명값으로 k값을 계산할 수 있다.  
이제 s식을 통해 개인키 dS를 계산할 수 있다.

> s =  k^-1(z+r*dA)mod => dS = (r^-1)(s*k-z) mod n










