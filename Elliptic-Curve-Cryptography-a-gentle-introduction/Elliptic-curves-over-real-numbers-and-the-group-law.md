# 타원곡선 암호화: 간단한 소개 #

---

공개키 암호화를 아는 사람이라면 ECC, ECDH 또는 ECDSA에 대해 들어봤을 것이다. 

ECC는 Elliptic Curve Cryptography의 약어이고 ECDH, ECDSA는 ECC를 기반으로 하는 알고리즘이다. 

비트코인 및 기타 암호화폐 뿐 아니라, 현대의 웹 및 IT의 주요 기술인 [TLS](https://tools.ietf.org/html/rfc4492) , [PGP](https://tools.ietf.org/html/rfc6637) 및 [SSH](https://tools.ietf.org/html/rfc5656) 에서도 타원 곡선 암호 시스템 사용하고 있다.

ECC가 대중화되기 전에는 거의 모든 공개 키 알고리즘이 모듈식 산술(modular arithmetic)을 기반으로 하는 대체 암호 시스템인 RSA, DSA 및 DH를 기반으로 했다. 이 시스템은 여전히 매우 중요하며 종종 ECC와 함께 사용한다. 기존의 시스템은 쉽게 설명할 수 있고, 많은 사람들이 이해하고 있으며, [간단하게 구현할 수 있지만](https://code.activestate.com/recipes/578838-rsa-a-simple-and-easy-to-read-implementation/), ECC의 기초는 그렇지 않다. 

앞으로의 게시물을 통해 타원 곡선 암호화의 세계를 간단히 소개하려 한다. 

이 포스터의 목적은 ECC에 대한 완전하고 상세한 가이드를 제공하는 것이 아니라, 긴 수학적 증명이나 자세한 설명에 시간을 낭비하지 않고 **ECC가 무엇이며 왜 안전한지**, 또 **시각적 툴과 스크립트를 통해 유용한 예제를 함께 제공하는 것**이다.



구체적으로 다루게 될 주제는 다음과 같다. 

> 1. 실수 및 그룹 법칙에 대한 타원 곡선
> 2. 유한 필드에 대한 타원 곡선 및 이산 로그 문제
> 3. 키 쌍 생성 및 두 가지 ECC 알고리즘: ECDH 및 ECDSA
> 4. ECC 보안을 깨는 알고리즘 및 RSA와의 비교

위 주제를 이해하려면 집합 이론, 기하학 및 모듈식 산술의 몇 가지 기본적인 것들을 알아야 하고 대칭 및 비대칭 암호화에 익숙해야한다. 마지막으로, 무엇이 *쉬운* 문제인지, 무엇이 *어려운* 문제 인지, 그리고 그들이 암호학에서 어떤 역할을 하는 지 명확히 알아야 한다. 

준비가 되었다면 시작하자!

---

## Elliptic Curves: 타원 곡선 ##

### 무엇이 타원 곡선인가? ### 

Wolfram MathWorld는에서 타원 곡선을 완벽하게 [정의](https://mathworld.wolfram.com/EllipticCurve.html) 한다.  
하지만 우리의 목적에 맞게, 타원 곡선은 단순히 다음 방정식으로 설명되는 점의 집합이다. 

![equation](../images/equation.png)

단, [singular curve](https://en.wikipedia.org/wiki/Singularity_(mathematics)) 를 제외하기 위해 *4*a^3 + 27*b^2 != 0*을 만족해야한다.  
위의 방정식은 타원 곡선의 *Weierstrass normal form* 이다.



![curves](../images/curves.png)

![singularities](../images/singularities.png)

타원곡선은 a와 b의 값에 따라 다양한 모양을 가진다. 쉽게 확인할 수 있 듯, 타원곡선은 x축에 대하여 대칭이다.

우리의 목적에 부합하기 위해, 우리는 곡선의 일부로 [무한원점(Point at infinity 혹은 Ideal Point)](https://en.wikipedia.org/wiki/Point_at_infinity) 을 정의해야 한다.  
무한원점을 기호 0으로 표시한다.

무한원점을 명시적으로 표현하면 다음과 같이 타원 곡선을 정의한다.  
![equation-with-ideal-point](../images/equation-with-ideal-point.png)

---

## Groups: 그룹 ##

수학에서의 Group은 '덧셈'(+로 표현하는)으로 정의했던 이항 연산의 집합이다.  

집합 **G**가 group이 되기 위해서는 다음과 같은 4가지 속성을 만족하는 *덧셈*을 정의해야한다. 

> 1. **닫힘**: 만약 a와 b가 G의 원소라면, a+b는 G의 원소이다. 
> 2. **결합 법칙**: (a + b) + c = a + (b + c)
> 3. 항등원 0 이 존재한다. a + 0 = 0 + a = a
> 4. 모든 요소는 역원을 갖는다. 즉, 모든 a 에 대해 a + b = 0 을 만족하는 b 가 존재한다. 
 
5번째 속성을 추가한다면,

> 5. **연관 법칙**: a + b = b + a

이 속성을 모두 만족할 때, 이 group을 abelian group 이라 한다. 

일반적인 덧셈 개념으로 정수 집합 Z는 group이며 또한 abelian group이다. 자연수의 집합 N 은 4번째 속성을 만족할 수 없으므로 group 이라할 수 없다. 

Group이 좋은 이유는 이 네 가지 속성을 만족한다는 것을 입증할 수 있다면, 다른 속성을 쉽게 얻을 수 있기 때문이다.   
예를 들어, **항등원이 유일하다** . 또한 **역원이 유일하다** 같은 속성들을 쉽게 얻을 수 있다.  
즉, 모든a에 대하여 a + b = 0을 만족하는 b가 오직 하나 존재한다.(그러면 우리는 b를 -a로 쓸 수 잇다.).  
이러한 group에 대한 사실들은 후에 다룰 부분들에 있어 매우 중요하다.  

---

## The group law for elliptic curves ##

타원 곡선 위에서 group을 정의해보자. 

> 1. group의 원소들은 타원 곡선 위의 점이다.   
> 2. 항등원은 무한원점 0 이다.
> 3. 점 P의 역원은 x축에 대칭인 점이다.   
> 4. 덧셈은 다음과 같은 법칙에 따른다. 일직선 위에 주어진 0이 아닌 세 개의 점 P, Q, R에 대하여, 그들의 합 P + Q + R = 0 이다.

![three-aligned-points](../images/three-aligned-points.png)

마지막 법칙을 보면, 우리는 일직선 위의 세 개의 점만 필요하며, 그 세 점의 순서는 상광없다.  

즉, 점 P, Q, R이 일직선 위에 존재한다면, P + (Q + R) = Q + (P + R) = R + (P + Q ) = ... = 0 을 만족한다.  
이러한 방식으로, 우리는 직관적으로 우리의 + 연산자가 교환 법칙과 결합 법칙을 따른다는 것을 알 수 있다. 즉 이것이 **abelian group**이라는 것을 의미한다. 

**그렇다면 임의의 두 점의 합을 계산하는 방법은 무엇일까?** 

---

## Geometric addition: 기하적 덧셈 ##

abeilan group이라는 사실 덕분에, P + Q + R = 0을 P + Q = -R으로 쓸 수 있다.   
이 방정식에서 두 점 P 와 Q 사이의 합을 기하학적으로 계산할 수 있다.  
만약 우리가 P 와 Q를 지나는 직선을 그린다면, 이 직선은 곡선위의 제 3의 점을 지날 것이다. 이 점을 R 이라 하자. (이는 P, Q, R이 한 직선 위에 있음으로 부터 알 수 있다.)  
만약 우리가 이 점의 역을 취한다면, P + Q의 결과인 -R를 알 수 있다. 

![point-addition](../images/point-addition.png)

이 기하적인 방법은 약간의 개선이 필요하다. 특히 다음과 같은 몇가지 질문에 답해야 한다.
> 만약 P 또는 Q가 0 이라면? 

0은 xy 평면상에 존재하지 않으므로 어떠한 직선도 그릴 수 없다. 그러나 0을 항등원으로 정의한다면, 모든 P, Q에 대하여 P + 0 = P 이고 Q + 0 = Q을 만족한다.	

> 만약 P = -Q 라면? 

이 경우에, 두 점을 지나는 직선은 수직이다. 그리고 곡선과 교차하는 제 3의 점이 존재하지 않는다. 그러나 만약 P가 Q의 역원이라면, P + Q = P + (-P) = 0을 역원의 정의로부터 도출할 수 있다.

> 만약 P = Q 라면? 

이 경우에, 점을 지나는 무한히 많은 직선이 존재한다. 이 경우에 조금 복잡하다.   
P가 아닌 점 Q'를 생각해보자. 만약 Q'가 P에 점점 가까워진다면 어떻게 될까?   

![animation-point-doubling](../images/animation-point-doubling.gif)

Q'가 P에 매우 가까워지면, P와 Q'를 지나는 직선은 곡선에 접선이 된다.    
이 경우에 우리는 P + P = -R이라할 수 있다.(R은 곡선과 P에서의 접선과의 교점이다.)   
> 만약 P!=Q 일 때, P와 Q를 지나는 직선과 곡선의 다른 접점 R이 없다면?  

이 경우는 이전과 아주 비슷하다. 이 경우는 P와 Q를 지나는 직선이 곡선의 접선이 되는 경우이다. 

![animation-tangent-line](../images/animation-tangent-line.gif)  

P가 접점이라고 가정하자. 이전의 경우, P + P = -Q 였던 방정식은 P + Q = - P가 된다. 만약, Q가 접점이라면, P + Q = -Q가 될 것이다.  
이제 이 기하학적인 방법으로 모든 상황을 확인할 수 있다. 

[여기를](https://andrea.corbellini.name/ecc/interactive/reals-add.html) 확인하세요!

---

## Algebraic addition ##

점의 덧셈을 *계산*하고 싶다면, 기하적인 방법을 대수적인 방법으로 다뤄야 한다.   
위에 설명한 법칙들을 식으로 변환하는 것은 직관적이지만, 3차 방정식을 풀어야하기 때문에 너무 오래걸린다. 그래서 여기서는 결과만 설명한다.

먼저 가장 짜증나는 코너케이스 먼저 없애보자.  
이전에 두 개의 법칙 P + (-P) = 0 와 P + 0 = 0 + P = P 를 다뤘다.  
따라서 방정식에서 이 두가지 경우를 피하고 두 개의 0 이 아닌 비대칭 점 **P = (xp, yp), Q= (xq, yq)** 만 고려한다.

1. 우선 P와 Q가 서로 다른 점이라면( xp != xq), 두 점을 동시에 지나는 직선은 기울기가 있다.

![slope](../images/slope.png)

이 직선과 곡선이 만나는 또 다른 교점을 R = (xr, yr)이라고 하면, R은 다음과 같다.

![intersection](../images/intersection.png)

혹은,

![intersection-in-one-line](../images/intersection-in-one-line.png)

**따라서, (xp, yp) + (xq, yq) = (xr,-yr)를 만족한다!**


2. P와 Q가 같은 점이라면, 조금 다르다. 이때 직선이 지나는 기울기는 다음과 같다.

![slope-when-P=Q](../images/slope-when-P=Q.png)

이 식은 다음 식의 도함수에서 도출해낸다.

![derivative](../images/derivative.png)

결과를 검증하기 위해서는 R이 곡선과 직선을 모두 지나는 지 증명해야 하지만, 생략한다.

--- 

## Scalar multiplication ##  

이제 *덧셈* 말고 다른 연산을 정의해보자: **scalar multiplication**,

![scalar-multiplication](../images/scalar-multiplication.png)

n은 자연수이다. [여기를](https://andrea.corbellini.name/ecc/interactive/reals-mul.html) 참고하세요!  
위에서 정의한 식처럼, nP를 계산하는데 n번의 덧셈 연산이 필요하다.  
n이 k bit의 숫자라면, 연산한느데 O(2^k)의 시간이 들겠지만, 더 빠른 알고리즘이 있다.

그 중 하나다 **double and add** 알고리즘읻. 예시를 들면 알고리즘의 원리를 이해하기 쉽다.  
n을 151라고 하자. 이진수로 표현하면 10010111이다. 이 이진수 표현을 2의 제곱수로 풀어서 표현하면 다음과 같다.

![binary-representation](../images/binary-representation.png)

따라서 nP 또한 다음과 같다.

![binary-representation-of-nP](../images/binary-representation-of-nP.png)

double and add 알고리즘으로 위 식을 풀어가보면

> 1. P에서 시작한다.   
> 2. P에 2를 곱해 2P를 얻는다.  
> 3. 2P에 P를 더한다. (2^1*P + 2^0*P를 구하는 것이다.)  
> 4. 2P에 2를 곱해 2^2*P를 얻는다.  
> 5. 위 결과에 더한다. (2^2*P + 2^1*P + 2^0*P를 구하는 것이다.)  
> 6. 2^2*P에 2를 곱해 2^3*P를 얻는다.  
> 7. 이때는 덧셈을 하짐 않는다.(위 예시에서 nP식에 2^3*P는 없기 때문)  
> 8. 2^3P에 2를 곱해 2^4*P를 얻는다.  
> 9. 위 결과에 더한다. (2^4*P + 2^2*P + 2^1*P + 2^0*P를 구하는 것이다.)  
> 10. ...

**결과론적으로, 단 7번의 doubling과 4번의 덧셈으로 151P의 결과를 얻는다.**

덧셈과 곱셈이 O(1)이라면, 이 알고리즘의 시간복잡도는 O(log n) (bit의 길이까지 고려한다면 O(k))이다.

---

## Logarithm ##

n과 P를 알고 있다면, Q = nP를 계산하는데 다항시간이 든다는 걸 알았다. 반대 경우는 어떨까?  
**Q와 P를 알고 있을 때, n을 구할 수 있을까??**
이 문제는 **로그 문제(logarithm problem)**로 알려져 있다.

이 문제에 쉬운 알고리즘은 알려진게 없다. 다만 몇몇의 패턴은 존재한다. 우리가 타원곡선에 더 익숙해질수록 더 많은 패턴을 발견할 수 있을 것이고,
그 곡선 위에서 문제를 효율적으로 해결하는 방법을 찾을 수도 있다.

하지만 다양한 로그 문제가 있는데:이산로그 문제(discrete logarithm problem)이 그것이다.  
다음 포스터에서는 다루겠지만, **다루는 곡선을 한정하면 앞서 다룬 scalar multiplication은 '쉽지만', 이산로그문제는 '어렵다'.**  
이것이 타원곡선암호학의 핵심이다.
